from distutils.core import setup

setup(
    name='percolate',
    version='0.1',
    packages=[''],
    url='https://bitbucket.org/cvillarroel/percolate',
    license='license.txt',
    author='Cristobal Villarroel',
    author_email='cristobal23@gmail.com',
    description='funky rolodex coding challenge'
)
