import unittest
from percolate import line_parser

class PercolateTester(unittest.TestCase):
    def fake_open_file(self):
        pass

    def setUp(self):
        # self.p.o  pen_file = self.fake_open_file
        pass

    def tearDown(self):
        pass

    def test_line_parser_pass(self):
        line = 'Cristobal Villarroel, green, 510 290 4081, 94805'
        call = line_parser(line)
        result = {'firstname': 'Cristobal', 'lastname': 'Villarroel'}
        self.assertEquals(result, call)

    def test_line_parser_fail(self):
        self.assertEquals(None, line_parser(''))
        self.assertEquals(None, line_parser('12345'))