import sys

def line_parser(the_line):
    elements = the_line.split(',')
    if len(elements) == 4:
        name = elements[0].split(' ')
        try:
            first_name = name[0]
            last_name = name[1]
        except SyntaxError:
            first_name = None
            last_name = None
    elif len(elements) == 5:
        last_name = elements[0]
        first_name = elements[1].strip()
    else:
        return None
    return {'firstname': first_name, 'lastname': last_name}

def main(args):
    filename = 'sample-Cristobal.in'
    entries = []
    errors = []
    line_count = 0
    with open(filename) as file:
        for line in file:
            line_count += 1
            entry = line_parser(line)
            if entry is None:
                errors.append(line_count)
            else:
                entries.append(entry)
    print errors
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

"""
Assumptions:
Aldous, yellow, Huxley, 510 290 4081, 94805 won't happen
"""